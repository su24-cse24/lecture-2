#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>
#include <GL/gl.h>
#include "AppController.h"

class Controller : public AppController {

public:
    Controller(){
        // Initialize your state variables
    }

    void render() {
        // Render some graphics
    }

    // detecting left mouse down events
    void leftMouseDown(float x, float y) {
        std::cout << "Left Mouse Down: (" << x << ", " << y << ")" << std::endl;
    }

    void leftMouseUp(float x, float y) {
        std::cout << "Left Mouse Up: (" << x << ", " << y << ")" << std::endl;
    }

    void rightMouseDown(float x, float y) {
        std::cout << "Right Mouse Down: (" << x << ", " << y << ")" << std::endl;
    }

    void rightMouseUp(float x, float y) {
        std::cout << "Right Mouse Up: (" << x << ", " << y << ")" << std::endl;
    }

    void mouseMotion(float x, float y) {
        std::cout << "Mouse Motion: (" << x << ", " << y << ")" << std::endl;
    }

    void keyboardDown(unsigned char key, float x, float y) {
        std::cout << "Key: " << key << std::endl;
    }

    ~Controller(){
        // Release memory
    }
};

#endif