#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>
#include <GL/gl.h>
#include "AppController.h"

enum Laser {DOT, DISK, CROSS};

class Controller : public AppController {
    float px;
    float py;
    float pr;
    float pg;
    float pb;
    float pSize;
    Laser laser;

public:
    Controller(){
        // Initialize your state variables
        px = 0.0f;
        py = 0.0f;
        pr = 1.0f;
        pg = 0.0f;
        pb = 0.0f;
        pSize = 20.0f;
        laser = DOT;
    }

    void render() {
        // Render some graphics
        if (laser == DOT) {
            glPointSize(pSize);
            glColor3f(pr, pg, pb);

            glBegin(GL_POINTS);
                glVertex2f(px, py);
            glEnd();
        }
        if (laser == DISK) {
            glPointSize(pSize);
            glColor3f(pr, pg, pb);

            glBegin(GL_POINTS);
                glVertex2f(px, py);
            glEnd();

            glPointSize(pSize/2);
            glColor3f(1.0f, 1.0f, 1.0f);

            glBegin(GL_POINTS);
                glVertex2f(px, py);
            glEnd();
        }
        if (laser == CROSS) {
            glPointSize(pSize);
            glColor3f(pr, pg, pb);

            glBegin(GL_LINES);
                glVertex2f(px - pSize/400, py + pSize/400);
                glVertex2f(px + pSize/400, py - pSize/400);

                glVertex2f(px - pSize/400, py - pSize/400);
                glVertex2f(px + pSize/400, py + pSize/400);
            glEnd();
        }
    }

    void leftMouseDown(float x, float y) {
        std::cout << "Left Mouse Down (" << x << ", " << y << ")" << std::endl;
        px = x;
        py = y;
    }

    void mouseMotion(float x, float y) {
        std::cout << "Mouse Motion (" << x << ", " << y << ")" << std::endl;
        px = x;
        py = y;
    }

    void keyboardDown(unsigned char key, float x, float y) {
        if (key == 'r') {
            std::cout << "Change color to red" << std::endl;
            pr = 1.0f;
            pg = 0.0f;
            pb = 0.0f;
        }
        if (key == 'g') {
            std::cout << "Change color to green" << std::endl;
            pr = 0.0f;
            pg = 1.0f;
            pb = 0.0f;
        }
        if (key == 'b') {
            std::cout << "Change color to blue" << std::endl;
            pr = 0.0f;
            pg = 0.0f;
            pb = 1.0f;
        }

        if (key == 'i') {
            std::cout << "Increase point size" << std::endl;
            pSize += 2.5f;
        }

        if (key == 'd') {
            std::cout << "Decrease point size" << std::endl;
            pSize -= 2.5f;
        }

        if (key == 't') {
            if (laser == DOT) {
                laser = DISK;
            }
            else if (laser == DISK) {
                laser = CROSS;
            }
            else if (laser == CROSS) {
                laser = DOT;
            }
        }
    }

    ~Controller(){
        // Release memory
    }
};

#endif